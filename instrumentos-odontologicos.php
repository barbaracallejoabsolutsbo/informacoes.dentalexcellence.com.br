<?php
    $title       = "Instrumentos Odontológicos";
    $description = "Os instrumentos odontológicos da empresa Dental Excellence são os mais procurados do mercado. Venha saber o porque e tenha a certeza de que fez a escolha certa.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Garanta os melhores instrumentos odontológicos com a empresa Dental Excellence e tenha soluções completas para a sua necessidade. Somos uma empresa sólida e séria que não hesita em fazer um bom trabalho e nos destacamos no mercado odontológico, pois, oferecemos um atendimento personalizado e soluções eficientes.</p>

<p>Os instrumentos odontológicos são de suma importância que se mantenham organizados para que não sejam confundidos e facilitem o trabalho do profissional na hora de realizar o procedimento adequado. A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer o suporte completo, com presteza e atenção.</p>

<h2>Importância dos instrumentos odontológicos:</h2>

<p>De espelhos a sondas, os dentistas exigem precisão quando se trata de alcançar todos os cantos da boca; por isso, os instrumentos odontológicos devem fornecer excelente controle. </p>
<p>No entanto, é importante destacar que, os instrumentos odontológicos também podem parecer um pouco ameaçadoras. O medo de tratamentos dentários é a razão predominante entre os pacientes para evitar o atendimento odontológico. </p>
<p>Saber o que esperar do tratamento com os instrumentos odontológicos pode ajudá-lo a se sentir mais preparado para as visitas ao dentista. </p>
<p>Confira abaixo alguns instrumentos odontológicos mais comuns que você pode encontrar no dentista:</p>
<p>Espelho bucal: Este, entre os instrumentos odontológicos, ajuda os dentistas a compreender melhor a cavidade oral, observando atentamente cada canto da boca. O espelho bucal também serve como uma ferramenta para mover suavemente os tecidos da boca ou a língua para uma melhor visualização;</p>
<p>Sickle Probe: Também chamado de ‘explorador dental’, a sonda falciforme é usada para detectar problemas orais, como cáries. Embora estes instrumentos odontológicos venham em vários formatos e tamanhos, esta sonda é um bastão de metal e pode ter ganchos de diferentes formatos na extremidade. Os dentistas geralmente analisam a dureza da superfície dos dentes usando a sonda. Também pode ser usado para remover o tártaro e a placa bacteriana entre os dentes. </p>
<p>Scaler: Entre os instrumentos odontológicos, este se destacada, pois, é utilizado para remoção de placa bacteriana e tártaro Um scaler ajuda a lidar com problemas orais, como acúmulo de placa, doença periodontal e outras formas de acúmulo que não podem ser raspadas com uma sonda. A maioria desses acúmulos fica presa em pequenas bolsas entre os dentes. Embora a escovação e o uso do fio dental possam remover partes significativas deles, uma escova dental não pode atingir as áreas que um raspador pode alcançar para uma limpeza mais profunda. </p>
<p>Lembrando que, hoje em dia, existem instrumentos odontológicos chamados raspadores ultrassônicos disponíveis que removem os acúmulos sólidos usando energia vibracional em vez de raspagem. Atente-se, pois, se não forem tratados, esses problemas bucais podem causar doença periodontal ou cárie dentária.</p>
<p>Dispositivo de sucção: Existem basicamente dois tipos de dispositivos de sucção em instrumentos odontológicos usados ​​pelos dentistas. O ejetor de saliva usa um baixo nível de sucção e consiste em um tubo de plástico oco em forma de J e a outra ferramenta de sucção do dentista comumente usada é um dispositivo de sucção de alto nível para tártaro, fragmentos de dente e obturações antigas. </p>
<p>Broca dentária: Este, entre os instrumentos odontológicos é mais comum, pois, temo objetivo de remover efetivamente a cárie dentária antes de obturações de cavidades. A broca também inclui um mecanismo para borrifar um jato de água enquanto corta os dentes. A água impede que a broca fique muito quente, o que poderia causar danos aos dentes. A perfuração pode ser desconfortável porque envia vibrações para os dentes e gengivas. Embora possa ser doloroso, os dentistas tentam minimizar isso usando um anestésico local.</p>
<p>Bolores: Os dentistas geralmente exigem impressões de seus dentes para procedimentos odontológicos, como a colocação de coroas. Os moldes são pequenas armações preenchidas com líquido que endurece com o tempo, formando o contorno perfeito dos dentes. Embora o molde possa deixar um gosto amargo na boca, vale a pena. Uma cavidade é tratada com uma obturação, onde os dentistas limpam a cárie e usam uma resina composta para preencher a cavidade. O dentista usará uma variedade de instrumentos odontológicos, incluindo uma broca. O procedimento geralmente envolve anestesiar o dente e usar um anestésico local. </p>
<p>De forma sucinta, finalmente, após várias camadas, a resina preenche completamente a cavidade para restaurar a aparência natural do dente. O dentista usa também, entre os instrumentos odontológicos, o polidor para os retoques finais. Os custos do preenchimento dentário dependerão do tipo de preenchimento que você escolher.  Por isso, pesquise bem antes de tomar quaisquer decisões precipitadas.</p>
<h2>Vantagens dos instrumentos odontológicos da Dental:</h2>
<p>Deixe os detalhes conosco e desfrute de instrumentos odontológicos de qualidade. Com anos de experiência no ramo, nós contamos com uma equipe unida e organizada que busca não somente atender a sua necessidade, mas também, superar todas as suas expectativas.</p>
<p>Neste sentido, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição dos mais modernos instrumentos odontológicos. E ainda, é importante destacar que seguimos todas as normas regulamentadoras exigíveis para garantir a segurança de todos os envolvidos em nossa relação.</p>
<p>Nós trabalhamos todos os dias para entregar os melhores produtos com a melhor qualidade. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. O respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa.</p>
<p>A Dental Excellence se destaca por sua capacidade de se reinventar e criar produtos que chegaram ao mercado e expandiram sua presença de maneira expressiva. Devido ao seu compromisso de procurar sempre alcançar o máximo em qualidade com um custo benefício justo para o cliente final, a nossa equipe fornece com elevado nível de desenvolvimento tecnológico.</p>
<p>Por fim, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em instrumentos odontológicos sempre focando nos pacientes.</p>
<p>Está esperando o que para ligar agora mesmo, tirar todas as suas dúvidas e realizar um orçamento sem compromisso? A qualquer hora do dia estamos disponíveis para fornecer o suporte completo que o cliente procura e merece, com presteza e atenção. Venha conferir o nosso portfólio e tenha a certeza de que fez a escolha certa. A nossa missão é transformar todos os sorrisos do mundo em um só. Ligue agora mesmo e saiba mais sobre os nossos instrumentos odontológicos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>