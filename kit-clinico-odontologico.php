<?php
    $title       = "Kit Clínico Odontológico";
    $description = "Gostou e quer saber mais sobre o kit clínico odontológico da Dental Excellence? Não perca essa oportunidade, venha conferir todos os nossos detalhes e produtos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Garanta o kit clínico odontológico da Dental Excellence e tenha soluções completas a sua necessidade, de forma rápida e eficiente. Há mais de 25 anos atuando neste ramo, conquistamos o nosso espaço no mercado e buscamos superar as suas expectativas, sempre acompanhando as constantes evoluções do mercado.</p>
<p>O kit clínico odontológico nada mais é do que um conjunto de instrumentos auxiliares utilizados nos procedimentos clínicos odontológicos. Mas não se preocupe, pois, apesar de um amplo portfólio neste ramo, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas para fornecer todo o suporte necessário, com presteza e atenção.</p>
<h2>Por que ter um kit clínico odontológico?</h2>
<p>É de suma importância obter um kit clínico odontológico  em um exame físico realizado no atendimento odontológico, pois, um correto diagnóstico das lesões bucais é essencial para que a interpretação dos dados colhidos auxiliem no tratamento adequado a sua necessidade.</p>
<p>Em geral, ter um kit clínico odontológico emergência é muito importante em algumas ocasiões para que você possa aliviar a dor ou estancar o sangramento antes de ir ao dentista de verdade, por exemplo.</p>
<p>Aqui estão as ferramentas importantes que você pode incluir em kit clínico odontológico de primeiros socorros:</p>
<p>Suprimentos odontológicos gerais - Essas ferramentas são os conteúdos essenciais do kit clínico odontológico primeiros socorros para a prática básica, e incluem:</p>
<ul>
<li>
<p>Luvas - antes de ajudar os dentes de outra pessoa, você deve se certificar de que está usando luvas. Mantenha luvas de látex ou vinil em seu kit para poder tratar qualquer pessoa com segurança</p>
</li>
<li>
<p>Algodão - bolas de algodão e compressas de gaze são importantes para estancar o sangramento</p>
</li>
<li>
<p>Fio dental - se houver comida presa entre os dentes, pode causar um grande desconforto. Ajude a fornecer alívio durante esta instância, mantendo o fio dental em seu kit</p>
</li>
<li>
<p>Espelho dentário - os espelhos dentais são importantes para uma lesão mais grave ou emergência. </p>
</li>
</ul>
<p>Lembrando que, você não precisa praticar odontologia para ter pressa em uma emergência odontológica. Com a ajuda de conhecimentos básicos de odontologia e um kit clínico odontológico de emergência , você pode economizar muito para você e sua família, evitando ir ao dentista de primeiro momento para algo que você mesmo pode fazer. </p>
<p>Ter um kit clínico odontológico em casa é obrigatório para quem precisa de um auto check-up instantâneo. Com o uso de ferramentas familiares em uma clínica dentária, a pessoa consegue ver claramente o que precisa ser consertado antes mesmo de ir para uma consulta agendada. Existem diferentes tipos de kits odontológicos de acordo com a idade. </p>
<p>No entanto, o kit clínico odontológico faz parte do trabalho dos especialistas e clínicos gerais. Eles devem ser organizados e armazenados em embalagens apropriadas em armários fechados. A melhor forma de fazer isso é com a categorização. Ou seja, o kit clínico odontológico deve separar o fórceps, pinças, bandejas e sugadores, sendo identificados com uma etiqueta e a data da última esterilização.</p>
<p>Para exames clínicos, se faz necessário o uso do kit clínico odontológico e para isso eles devem obter:</p>
<ul>
<li>
<p>Espelho clínico: Observa a boca do paciente para o tratamento adequado;</p>
</li>
<li>
<p>Sonda exploradora: Identifica falhas na estrutura dental;</p>
</li>
<li>
<p>Pinça clínica: Auxilia no manejo dos materiais dentro ou fora da cavidade bucal, entre outros.</p>
</li>
</ul>
<p>Todos os instrumentos que compõem o kit clínico odontológico, assim como outros mais populares como as brocas, tesouras e bisturis, e aqueles bem específicos como os mais variados tipos de pinças, o sugador de sangue e o sindesmótomo, fazem parte, então, da rotina de profissionais de odontologia.</p>
<h2>Venha conferir o amplo portfólio de kit clínico odontológico da Dental Excellence!</h2>
<p>Todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. Além disso, visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição do melhor kit clínico odontológico.</p>
<p>Com foco e determinação, nós conquistamos o nosso espaço no mercado e, desde o início, estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados ao kit clínico odontológico sejam cumpridos à risca. </p>
<p>A nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos para kit clínico odontológico sempre focando nos pacientes. Atualmente, somos referência em tratamentos dentários de alta qualidade, com foco na valorização e humanização dos pacientes. Quem nos conhece pode confirmar a nossa excelência desde o atendimento personalizado que oferecemos até o serviço completo.</p>
<p>Entre os requisitos que seguimos para fornecer um bom kit clínico odontológico, podemos destacar:</p>
<ul>
<li>
<p>Compromisso com o cliente;</p>
</li>
<li>
<p>Competitividade na busca por soluções criativas e inovadoras;</p>
</li>
<li>
<p>Motivação na busca pelo bom desempenho profissional;</p>
</li>
<li>
<p>Respeito aos clientes, fornecedores e com os que trabalham conosco.</p>
</li>
</ul>
<p>O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas e terá a certeza de que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. </p>
<p>É importante destacar que, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos à risca. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas e terá a certeza de que encontrou a empresa ideal chamada Dental Excellence para se tornar o mais novo parceiro de longa data.</p>
<p>Nós garantimos seguir as regras e regulamentações rígidas dentro do segmento de kit clínico odontológico, com todos os registros exigidos pela Anvisa (Agência Nacional de Vigilância Sanitária) e está dentro de todos os padrões de qualidade estipulados para esse mercado. Afinal, qual é o sentido de fechar negócio com um fornecedor com um excelente preço e materiais de qualidade, mas que, com frequência, não entrega tudo o que foi solicitado em um pedido? Deixe os detalhes conosco, ligue agora mesmo e saiba mais sobre o nosso kit clínico odontológico.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>