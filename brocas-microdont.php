<?php
    $title       = "Brocas Microdont";
    $description = "A Dental Excellence é renomada no ramo de tratamentos dentário de alta qualidade, incluindo as brocas microdont. Venha conferir agora mesmo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Está procurando por brocas microdont de qualidade? Bem vindo a Dental Excellence, uma empresa sólida e séria que conquistou o seu espaço no mercado, apresentando produtos de ótima qualidade e soluções completas a necessidade de cada cliente, de forma totalmente personalizada. Além disso, a nossa equipe trabalha de forma unida e organizada, acompanhando as constantes evoluções do mercado para fornecer o que há de mais moderno.</p>

<p>Atualmente, as brocas microdont se destacam no ramo, pois, são produzidas com diamante natural e um aço inoxidável disponível em todo o mercado, inclusive internacional, garantindo maior eficiência no procedimento realizado. Mas não se preocupe, pois, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas referente às brocas microdont.</p>

<h2>Maiores informações das brocas microdont:</h2>
<p>É importante destacar que as brocas microdont são fabricadas seguindo alguns requisitos, entre eles, podemos citar:</p>
<p>• Valorização do lado humano;<br />• Estabelecimento de metas e objetivos concretos;<br />• Prioridade no bom relacionamento com seu mercado.</p>
<p>As brocas microdont consistem em pontas diamantadas que são instrumentos rotatórios abrasivos utilizados principalmente na odontologia restauradora para realização de preparos dentários e acabamentos de restaurações. </p>
<p>Estas brocas microdont são formadas por uma haste metálica, eixo intermediário e ponta ativa, com o objetivo de mensurar a resistência flexural da haste e intermediário de pontas diamantadas.</p>
<p>É importante salientar que, a ISO indica a existência de duas normas relacionadas com instrumentos odontológicos rotatórios, como as brocas microdont. A norma 1797 refere-se ao diâmetro das hastes; e a norma 2157 refere-se ao diâmetro da ponta ativa. Entre vários testes de qualidade das brocas microdont e propriedades físico mecânicas de todos os produtos odontológicos, a resistência à flexão três pontos é enfatizada, pois está fortemente à fratura, que supostamente é capaz de prever o desempenho clínico da amostra. </p>
<p>Este teste das brocas microdont é utilizado por vários autores na área odontológica, pois fornece dados quantitativos sobre de formação e resistência à deformação. </p>
<p>Os resultados de testes laboratoriais com consequente determinação e/ou conhecimento das propriedades mecânicas de resistência das brocas microdont é de suma importância ao cirurgião dentista, pois permite optar trabalhar com materiais de melhor qualidade e maior segurança.</p>
<p>Na rotina clínica, o profissional odontológico pode encontrar problemas relacionados às pontas diamantadas, como inclinação (dobra), ou fratura destas, possibilitando a presença de fragmentos retidos na turbina de alta rotação. Nesta situação, o motor das brocas microdont deverá ser encaminhado para o serviço técnico para remover os fragmentos, sendo necessário também o descarte da ponta, que é considerado oneroso ao profissional. </p>
<p>O cirurgião dentista realiza diariamente procedimentos que necessitam do uso de brocas microdont de alta qualidade. Caso ele utilize pontas que não possuem alta resistência flexural, é possível que a ponta possa fraturar ou dobrar. </p>
<p>A não padronização das dimensões das brocas microdont e dos rolamentos dos motores de alta rotação é outro problema que pode interferir na vida útil do material, podendo promover falhas como fratura ou dobras. Deve-se ressaltar que independente da resistência flexural suportada por causa da ponta, ao haver a falha, a fratura seria mais prejudicial, quando comparado com a dobra. O problema complicaria se a fratura ocorresse na haste metálica, pois dificultaria a remoção com o sistema saca brocas microdont.</p>
<p>Diante de todos esses fatores técnicos, a marca Microdont destaca-se nos três testes, apresentando os melhores resultados estatísticos para a resistência de brocas microdont</p>
<p>Não há padronização da resistência flexural entre as marcas comerciais, sendo que a utilização de marcas com baixa resistência pode ocasionar em maiores taxas de falha. Por isso, conte com as brocas microdont.</p>
<h2>Conheça as brocas microdont fornecidas pela Dental Excellence:</h2>
<p>Conosco, você tem a tranqüilidade de receber brocas microdont de qualidade, através de uma equipe excepcional que está disponível para fornecer o suporte completo a sua necessidade, de forma rápida e eficiente. Além disso, é importante destacar que as nossas brocas microdont possuem valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição.</p>
<p>A nossa missão é comercializar produtos que proporcionem mais qualidade de vida ao ser humano. Oferecendo aos clientes uma linha de produtos reconhecida pela sua qualidade e com excelente custo benefício. Além de garantir a remuneração adequada daqueles que investem e trabalham pela empresa.</p>
<p>Entre os requisitos que seguimos para fornecer um bom serviço, podemos destacar:</p>
<ul>
<li>
<p>Compromisso com o cliente;</p>
</li>
<li>
<p>Competitividade na busca por soluções criativas e inovadoras;</p>
</li>
<li>
<p>Motivação na busca pelo bom desempenho profissional;</p>
</li>
<li>
<p>Respeito aos clientes, fornecedores e com os que trabalham conosco.</p>
</li>
</ul>
<p>Além dos fatores supracitados, ressaltamos que respeitamos e temos responsabilidade social e ambiental, além de uma ética responsável pela conduta empresarial.</p>
<p>Com foco e determinação, nós conquistamos o nosso espaço no mercado e, desde o início, estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos à risca. </p>
<p>O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas referente e terá a certeza de que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. </p>
<p>Por fim, vale ressaltar que, há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para entregar os melhores produtos com os melhores produtos. A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia.  Está esperando o que para entrar em contato conosco agora mesmo e realizar um orçamento sem compromisso? A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas. Deixe os detalhes conosco e desfrute de um trabalho bem feito. A nossa missão, principalmente, é transformar todos os sorrisos em um mundo só. Se interessou e quer saber mais? Ligue agora mesmo e tenha um atendimento totalmente personalizado. Esperamos por você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>