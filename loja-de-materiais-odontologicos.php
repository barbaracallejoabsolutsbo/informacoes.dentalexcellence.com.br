<?php
    $title       = "Loja de Materiais Odontológicos";
    $description = "A solução completa para o seu consultório é a loja de materiais odontológicos Dental Excellence, preços baixos, facilidade de pagamento, brindes e muito mais. Venha conferir.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Está procurando por uma loja de materiais odontológicos? Bem vindo a Dental Excellence, uma empresa sólida e séria que não hesita em fazer um bom trabalho e está sempre atenta às atualizações do mercado para fornecer o que há de melhor e mais moderno aos clientes.</p>
<p>Vale salientar que, em nossa loja de materiais odontológicos nós prezamos pelo bem estar completo dos clientes, por isso, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos à risca. E ainda, a qualquer hora do dia, estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2>Importância de uma boa loja de materiais odontológicos:</h2>
<p>Os materiais odontológicos fornecidos por uma loja de materiais odontológicos devem ser de qualidade, pois, são essenciais para uma entrega segura e de qualidade no serviço, por isso é importante escolher bem os equipamentos. Eles devem ser escolhidos conforme a especialidade do dentista: estética, ortodontia, endodontia e implantodontia. </p>
<p>No caso da clínica geral, um dos materiais mais vendidos na loja de materiais odontológicos é a autoclave que esteriliza os instrumentos de metal, canetas de alta rotação, vidro, papel, plástico, tecido, algodão e embalagens, por meio de vapor e da alta pressão. Enquanto, o compressor odontológico fornece ar comprimido para o jato de profilaxia, sugador e micromotor.</p>
<p>A qualidade dos instrumentos da loja de materiais odontológicos é de suma importância, pois, começa com a limpeza e a manutenção apropriada de todos os equipamentos utilizados.</p>
<p>A loja de materiais odontológicos deve ter assepsia e é importante tanto para saúde dos seus pacientes quanto para a sua própria saúde.</p>
<p>Por isso, optar por uma loja de materiais odontológicos é garantir as boas práticas em relação aos instrumentos e servem para proteger a imagem do consultório e construir uma relação de confiança entre o dentista e os pacientes.</p>
<p>Veja a seguir outros equipamentos e materiais servidos na loja de materiais odontológicos:</p>
<ul>
<li>
<p>lâmpadas LED ou halógenas;</p>
</li>
<li>
<p>aparelho de raio X;</p>
</li>
<li>
<p>materiais para restaurações, tratamento de cáries, coroas;</p>
</li>
<li>
<p>cimento;</p>
</li>
<li>
<p>adesivos;</p>
</li>
<li>
<p>fotopolimerizador.</p>
</li>
</ul>
<p>Também é fornecido pela loja de materiais odontológicos, o gel clareador, resina composta, brocas rotacionadas por motores, lima, guta-percha, reembasador, alginato, entre outros. O tipo de material utilizado para os tratamentos é diferente conforme cada caso específico. </p>
<p>O primeiro passo para adquirir os instrumentos odontológicos da maneira certa da loja de materiais odontológicos é usar as luvas adequadas.</p>
<p>Lembrando que, as luvas de procedimento oferecidas pela loja de materiais odontológicos não evitam o risco de contaminação totalmente. Elas devem ser substituídas pelas luvas de utilidade, mais resistentes a perfurações.</p>
<p>O detergente utilizado deve ser enzimático, fazendo sempre a imersão em uma cuba apropriada antes de esfregar os materiais com uma escova de cabo longo e cerdas macias.</p>
<p>Todos esses cuidados são necessários para garantir que a matéria orgânica acumulada sobre a superfície dos equipamentos seja totalmente retirada, evitando a proliferação dos micro-organismos infectantes. Por isso, opte por equipamentos de uma boa loja de materiais odontológicos, com a limpeza correta dos instrumentos odontológicos é que a esterilização será realmente efetiva.</p>
<h2>Loja de materiais odontológicos de qualidade é na Dental!</h2>
<p>A nossa política como loja de materiais odontológicos de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes.</p>
<p>Todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. Além disso, visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição com a melhor loja de materiais odontológicos.</p>
<p>O respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas e terá a certeza de que encontrou a loja de materiais odontológicos ideal para se tornar o mais novo parceiro de longa data.</p>
<p>Com foco e determinação, conquistamos o nosso espaço no mercado e, desde o início, estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados como loja de materiais odontológicos sejam cumpridos à risca. </p>
<p>A maior vantagem em contar com uma boa loja de materiais odontológicos é a de garantir que algumas despesas e tarefas que podem ser pesadas no final do mês para o profissional que trabalha sozinho seja evitada por um longo tempo, através dos materiais resistentes oferecidos.</p>

<p>Caso você opte por abrir um consultório sozinho, terá de arcar com todos os custos, por isso, nos procure como loja de materiais odontológicos. Além do mais, trabalhando sozinho você precisará de um número considerável de pacientes para que você consiga pagar suas contas e ter lucratividade e nós facilitamos isso pra você.</p>

<p>Mantenham em mente que, ao abrir um consultório odontológico em sociedade é também uma boa opção, deve haver especialistas de diversas áreas trabalhando juntos, a fim de evitar a concorrência interna. </p>

<p>Caso um paciente comece o tratamento com um dentista que possui um tipo de especialização e acabe precisando de outros cuidados, os colegas pode indicar um ao outro, através dos melhores equipamentos adquiridos em nossa loja de materiais odontológicos. Assim, uma vez que os especialistas estão em um mesmo estabelecimento, o paciente se sente confiante e aceita a recomendação. O benefício acontece para o profissional que recebe o colega também, já que ele acaba oferecendo uma outra especialidade dentro de sua clínica.</p>

<p>Por fim, vale ressaltar que, há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para entregar os melhores produtos com os melhores produtos. A nossa loja de materiais odontológicos é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Se interessou? Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Ligue agora mesmo e faça um orçamento sem compromisso.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>