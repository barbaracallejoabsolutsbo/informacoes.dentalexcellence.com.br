<?php
    $title       = "Instrumentais de Cirurgia Odontológica";
    $description = "Identificar os instrumentais de cirurgia odontológica é uma etapa primordial, para evitar principalmente que eles sejam trocados ou confundidos. Venha conferir.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os instrumentais de cirurgia odontológica são específicos e auxiliam o cirurgião dentista durante o procedimento, tendo diversos nomes e utilidades. Lembrando que, é fundamental que o auxiliar do dentista tenha conhecimento sobre todos os instrumentais de cirurgia odontológica e suas funções.</p>
<p>Sabendo dessa importância, a Dental Excellence se destaca no ramo, apresentando os melhores e mais modernos instrumentais de cirurgia odontológica, tendo em vista que está sempre atenta às atualizações do mercado para fornecer o que há de melhor.</p>
<h2>Conheça alguns instrumentais de cirurgia odontológica:</h2>
<p>Em geral, os instrumentais de cirurgia odontológica possuem a função de possibilitar os procedimentos ao odontólogo ao atendimento do paciente. </p>
<p>Os instrumentais de cirurgia odontológica para exames clínicos são:</p>
<ul>
<li>
<p>Bandeja;</p>
</li>
<li>
<p>Espelho clínico;</p>
</li>
<li>
<p>Sonda exploradora;</p>
</li>
<li>
<p>Pinça de algodão;</p>
</li>
<li>
<p>Escavador de dentina, entre outros.</p>
</li>
</ul>
<p>É importante destacar que, assim como em outros ramos, na odontologia, há os instrumentais de cirurgia odontológica principais e mais utilizados. Por isso, vamos conhecê-los melhor abaixo.</p>
<ul>
<li>
<p>Seringa Carpule: Além de ser utilizada possui como função primordial realizar o procedimento anestésico de diversos tratamentos;</p>
</li>
<li>
<p>Cureta: Atua principalmente na remoção de dentárias cariadas, visando remover a cárie e avaliar o grau do dano causado por ela;</p>
</li>
<li>
<p>Condensadores de canal: Serve para realizar a condensação vertical quando o dentista está realizando uma obturação;</p>
</li>
<li>
<p>Esculpidor Hollemback: Apropriado para fazer inserção de material pastoso, como curativos e resinas, na cavidade do dente;</p>
</li>
<li>
<p>Calcadores espatulados: São utilizados no preenchimento das cavidades dentárias que estiverem sendo tratadas;</p>
</li>
<li>
<p>Brocas: São utilizadas na remoção de material cariado e na preparação de cavidades dentais para receber restaurações.</p>
</li>
</ul>
<p>Além dos instrumentais de cirurgia odontológica supracitados, existem outros que também são imprescindíveis para o trabalho do dentista e fazem parte da rotina do consultório odontológico. Por isso, é importante contar com profissionais que saibam exatamente a função de cada um dos instrumentais de cirurgia odontológica para os procedimentos realizados.</p>
<p>É de suma importância que os instrumentais de cirurgia odontológica sejam além de facilitar no momento do atendimento ao paciente ou ao estar realizando algum procedimento, reduz também as chances de perdas e de estragos nos instrumentos.</p>
<p>Durante um procedimento, muitos instrumentais de cirurgia odontológica e equipamentos são utilizados. Por isso é essencial que eles estejam acessíveis e organizados. Uma dica para organizá-los é começar por setor (ortodontia, limpeza bucal, reconstrução), categorizar e depois identificar tantos os produtos quanto os instrumentos que você utiliza diariamente.</p>
<p>Para garantir que todos fiquem organizados e sejam encontrados assim que necessário, é primordial saber todos os instrumentais de cirurgia odontológica que compõem a rotina de trabalho.</p>
<h2>Pensou em instrumentais de cirurgia odontológica, pensou na Dental!</h2>
<p>Aqui, a prioridade é a sua satisfação completa. Por isso, os nossos instrumentais de cirurgia odontológica são todos registrados e organizados em planilhas, documentos de texto, softwares próprios ou, ainda, em aplicativos específicos para esse tipo de registro.</p>
<p>Vale ressaltar que, pensando em seu bem estar completo, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição dos melhores instrumentais de cirurgia odontológica. E ainda, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos à risca.</p>
<p>Agora que você conheceu os principais instrumentais de cirurgia odontológica, veja a maneira como cuidamos de nossos equipamentos:</p>
<ul>
<li>
<p>Efetuamos a desinfecção dos instrumentais de cirurgia odontológica através de agentes químicos, estando atento ao tempo de imersão e a diluição da solução;</p>
</li>
<li>
<p>Limpamos os instrumentais de cirurgia odontológica podendo fazer uso de escovas de cerdas e de aço, pois removem as sujeiras mais facilmente;</p>
</li>
<li>
<p>Enxaguar cuidadosamente com água potável;</p>
</li>
<li>
<p>Fazemos a secagem dos instrumentais de cirurgia odontológica para que eles não fiquem úmidos por bastante tempo;</p>
</li>
<li>
<p>Após o processo de secagem, inspecionamos os objetos a fim de verificar se não restou alguma impureza;</p>
</li>
<li>
<p>Por fim, utilizamos a autoclave para fazer a esterilização.</p>
</li>
</ul>
<p>A nossa missão é comercializar produtos que proporcionem mais qualidade de vida ao ser humano. Oferecendo aos clientes uma linha de produtos reconhecida pela sua qualidade e com excelente custo benefício. Além de garantir a remuneração adequada daqueles que investem e trabalham pela empresa.</p>
<p>O respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. Trabalhamos todos os dias para entregar os melhores produtos com os melhores preços e com a qualidade de atendimento que o cliente merece, contando com uma equipe treinada e especializada na área para que possam tirar as dúvidas sobre o nosso catálogo de produtos.</p>
<p>Por fim, vale ressaltar que, há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para entregar os melhores produtos com os melhores produtos. A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>Diante de todos esses fatores, fica evidente que somos a empresa ideal a sua necessidade. A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia.  Está esperando o que para entrar em contato conosco agora mesmo e realizar um orçamento sem compromisso? A qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas. Deixe os detalhes conosco e desfrute de um trabalho bem feito. A nossa missão, principalmente, é transformar todos os sorrisos em um mundo só. Se interessou e quer saber mais? Ligue agora mesmo e tenha um atendimento totalmente personalizado. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você; Esperamos por você para fazer um orçamento sem compromisso de nossos instrumentais de cirurgia odontológica.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>