<?php
    $title       = "Aparelho Fotopolimerizador";
    $description = "Com certeza o aparelho fotopolimerizador da Dental Excellence e o suporte oferecido por esta são incomparáveis aos demais, através de soluções completas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A bateria recarregável do aparelho fotopolimerizador é mais moderno e prático, pois, ele possui uma bateria interna que é recarregada sempre que o aparelho fotopolimerizador é encaixado em sua base, assim, a mesma fica constantemente ligada a tomada, como um telefone sem fio. A vantagem é a maior mobilidade e ausência de fios. Porém, a desvantagem é que o aparelho pode descarregar no meio de uma consulta, caso a base descarregue.</p>
<p>De forma conclusiva, com função cada vez mais importante nos consultórios e clínicas odontológicas, o aparelho fotopolimerizador emite uma luz, geralmente azulada, que ativa a canforoquinona que está presente nos compostos de resina e em outros produtos odontológicos, ajudando a endurecer os tecidos. </p>
<p>O mercado odontológico oferece diversas marcas, tipos e modelos, mas, para escolher o melhor aparelho fotopolimerizador é preciso levar em conta suas principais características: capacidade de cura rápida, segura, durável e uniforme; controle do tempo e potência; alcance; ergonomia e leveza, além da uniformidade do feixe de luz. Consulte um profissional.</p>
<h2>Saiba mais sobre o aparelho fotopolimerizador:</h2>
<p>Em geral, o aparelho fotopolimerizador está presente em todos os consultórios de odontologia. Atualmente, é conhecido por causa de sua luz azul e pela sua aplicação nas restaurações diretas, indiretas e na colagem dos braquetes.</p>
<p>Alguns tratamentos que utilizam o aparelho fotopolimerizador são:</p>
<ul>
<li>
<p>Fotopolimerização de resinas compostas;</p>
</li>
<li>
<p>Selante dental;</p>
</li>
<li>
<p>Restauração com resina;</p>
</li>
<li>
<p>Colagem de braquetes e outros acessórios ortodônticos;</p>
</li>
<li>
<p>Clareamentos dentais, entre outros.</p>
</li>
</ul>
<p>Além do aparelho fotopolimerizador possuir lâmpadas diferentes, ele também é dividido por modelos que tem fontes de energia diferentes, que não atrapaplha o desempenho do produto ou a sua qualidade. Lembrando que, a escolha do aparelho fotopolimerizador irá depender mais da preferência do dentista.</p>
<p>É importante salientar que há dois tipos de carga para o aparelho fotopolimerizador. Entre eles, podemos citar:</p>
<ul>
<li>
<p>Ligado a uma fonte de energia -  Esse tipo de aparelho fotopolimerizador é mais antigo e ele fica o tempo todo ligado a uma fonte de energia, tipo uma tomada. A maior vantagem desse modelo de aparelho fotopolimerizador é que o dentista não precisa colocar o aparelho pra carregar ou ficar preocupado se ele irá desligar bem no meio de um processo ou consulta.</p>
</li>
<li>
<p>Bateria recarregável - Esse modelo de aparelho fotopolimerizador vem com uma bateria em seu interior, que é recarregada quando o aparelho está encaixado na base. A vantagem desse modelo é que não possui fio para limitar, ou seja, sua mobilidade é muito maior e mais prática que a do outro modelo de aparelho fotopolimerizador.</p>
</li>
</ul>
<p>Atualmente, o aparelho fotopolimerizador intensidades mais altas, que permite durações mais curtas de cura para uma determinada profundidade de cura ou maior profundidade de cura para uma determinada duração de procedimento.</p>
<p>O uso das luzes do aparelho fotopolimerizador de maior intensidade pode, no entanto, produzir tensões de contração mais altas dentro da restauração. É importante lembrar que o tipo de aparelho fotopolimerizador e o procedimento usado ​​afeta a cinética de polimerização, a contração de polimerização e as tensões associadas, microdureza, profundidade de cura, grau de conversão, mudança de cor e microinfiltração em restaura ativadas por luz visível. </p>
<p>Vale salientar que, os problemas que podem estar associados a compósitos à base de resina ativados por luz incluem polimerização em direção à fonte de luz, sensibilidade do compósito à luz ambiente e variabilidade na profundidade da polimerização devido à intensidade de penetração da luz. </p>
<p>Podemos concluir que o aparelho fotopolimerizador é um aparelho bastante útil e que deve estar presente em todos os consultórios de odontologia, tendo em vista que é bastante prático e desempenha inúmeras funções importantes.</p>
<h2>O melhor aparelho fotopolimerizador está na Dental Excellence!</h2>
<p>Primeiramente, vale destacar que, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados no aparelho fotopolimerizador sejam cumpridos à risca. Além disso, prezamos pelo bem estar completo de nossos clientes e por isso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição.</p>
<p>Há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para entregar os melhores produtos com a melhor qualidade. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. </p>
<p>A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>Não se preocupe, pois, o nosso fotopolimerizador pode ser usado em vários materiais dentários diferentes, devido ao seu tipo de luz e dependendo do procedimento que o dentista planeja ao paciente. Lembrando que, existem quatro tipos básicos de fontes de luz de cura dentária: halogênio de tungstênio , diodos emissores de luz (LED), arcos de plasma e lasers. </p>
<p>Salientamos ainda que, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes.</p>
<p>O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas e terá a certeza de que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. Deixe os detalhes conosco e desfrute de um trabalho bem feito. Atualmente, somos referência em tratamentos dentários de alta qualidade, com foco na valorização e humanização dos pacientes. Está esperando o que para ligar agora mesmo, tirar todas as suas dúvidas e realizar um orçamento sem compromisso? A qualquer hora do dia estamos disponíveis para fornecer o suporte completo que o cliente procura e merece, com presteza e atenção. Venha conferir todos os nossos produtos de última geração e garantir um sorriso mais bonito para os seus clientes. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>