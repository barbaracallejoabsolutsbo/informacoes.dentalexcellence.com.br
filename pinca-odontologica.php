<?php
    $title       = "Pinça Odontológica";
    $description = "Conheça as ferramentas que o dentista usa para cuidar do seu sorriso e garanta-os com a Dental Excellence, inclusive a pinça odontológica. Esperamos por você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça a pinça odontológica da Dental Excellence e tenha a certeza de que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. Há mais de 25 anos atuando no mercado odontológico, contamos com uma equipe unida e organizada para fornecer o atendimento personalizado que o cliente procura e merece.</p>

<p>A pinça odontológica nada mais é do que um instrumento empregado para pegar rolinhos de algodão, brocas e gases, dando mais precisão ao dentista. Mas não se preocupe, pois, caso haja dúvidas, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção. </p>

<h2>Mais informações da pinça odontológica:</h2>

<p>De forma sucinta, a pinça odontológica é um dos instrumentos essenciais para a realização de intervenções cirúrgicas, destacando-se como uma ferramenta médica que auxilia na remoção de pontos, realização de exames e execução de curativos e procedimentos variados.</p>

<p>Vale salientar que, há diferentes tipos de pinças cirúrgicas, cada qual indicada para diversos tipos de abordagens. O que determina qual pinça odontológica será utilizada é o tipo de tecido em que será feita a intervenção.</p>

<p>Vamos conhecer abaixo alguns modelos de pinça odontológica e suas funções:</p>

<ul>
<li>
<p>Pinça odontológica de Adson: Indicada para manipulação de tecidos delicados, como a pele, e pode ter dois tipos de ponta: dentada ou lisa. Em sua utilização, ela retém parte do tecido enquanto o cirurgião manipula outros instrumentos, como a tesoura e o bisturi.</p>
</li>
</ul>

<ul>
<li>
<p>Pinça odontológica de Allis: Deve ser utilizada para a manipulação de tecidos mais robustos e músculos, além de auxiliar na remoção de tecidos do organismo.</p>
</li>
</ul>

<ul>
<li>
<p>Pinça odontológica Kocher: Pode ser reta ou curva, mas só deve ser utilizada em casos em que as outras pinças realmente não funcionem, uma vez que o risco de causar traumas ao tecido é maior.</p>
</li>
</ul>

<ul>
<li>
<p>Pinças hemostáticas traumáticas: São utilizadas para a manipulação de vasos sanguíneos, e podem ser classificadas entre traumáticas e não traumáticas, tendo em vista que sua função é induzir a hemostasia temporária, um processo fisiológico que consiste no bloqueio de lesões vasculares para interromper sangramentos e hemorragias.</p>
</li>
</ul>

<ul>
<li>
<p>Pinças hemostáticas não traumáticas: São utilizadas para obstruir temporariamente o fluxo sanguíneo por meio do pinçamento, com a finalidade de reduzir a possibilidade de trauma vascular na cirurgia.</p>
</li>
</ul>

<p>Os instrumentos odontológicos utilizados pelo dentista e escolhidos com critério por seu auxiliar visam à melhoria e ao aperfeiçoamento dos mais diversos procedimentos realizados no consultório dentário. Hoje em dia, a pinça odontológica é bastante específica, desenvolvida para suprir as necessidades do cirurgião dentista, nas mais diversas especialidades, como cirurgia, periodontia, ortodontia, implantodontia, entre outras.</p>

<p>Há diversos instrumentos utilizados além da pinça odontológica. Confira abaixo:</p>

<ul>
<li>
<p>O espelho é utilizado em exames clínicos, pode ser plano ou côncavo, para aumentar a imagem refletida. Serve para o dentista visualizar em detalhes as condições em que se encontra a boca do paciente </p>
</li>
</ul>
<ul>
<li>
<p>O Hollemback é apropriado para fazer inserção de material pastoso, como curativos e resinas, na cavidade do dente.</p>
</li>
<li>
<p>O fórceps é utilizado para a extração de dentes de leite e os diversos tipos de dentes definitivos, da mandíbula e do maxilar. </p>
</li>
<li>
<p>As brocas são utilizadas na remoção de material cariado e na preparação de cavidades dentais para receber restaurações. </p>
</li>
<li>
<p>Os calcadores espatulados são utilizados no preenchimento das cavidades dentárias que estiverem sendo tratadas com material curativo ou cimentos de vedação.</p>
</li>
</ul>

<p>Assim como a pinça odontológica, outros imprescindíveis instrumentos são necessário para o trabalho do dentista na rotina do consultório odontológico, como os condensadores, os brunidores, as espátulas para resinas, o porta-matriz, os fios afastadores, entre outros.</p>

<p>Por fim, ressaltamos que usar a pinça odontológica certa e adequada a sua especialização é essencial para promover atendimentos mais rápidos, precisos, de maneira mais simples e com custos reduzidos.</p>

<h2>Quais as vantagens da pinça odontológica da Dental Excellence?</h2>
<h2></h2>
<p>Além do melhor custo benefício do mercado, a nossa pinça odontológica possui diversas formas de pagamento para facilitar a sua aquisição. Além disso, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados na pinça odontológica sejam cumpridos à risca.</p>

<p>Conosco, a pinça odontológica é fornecida e sintetizada em nossas instalações, com respeito aos mais altos padrões de qualidade do início do processo até a chegada do material às mãos do profissional. Além disso, é importante frisar que seguimos todas as normas regulamentadoras exigíveis para garantir a segurança e confiança entre todos os envolvidos na relação.</p>

<p>Tenha a tranquilidade de comprar uma pinça odontológica com quem está disposto a te oferecer o melhor atendimento, caso tenha algum problema após a compra, conte com nossa equipe de pós-venda para solucioná-lo. Além disso, vale frisar que prezamos pelo bem estar completo de nossos clientes e por isso, trabalhamos de forma unida e organizada para superar as suas expectativas.</p>

<p>Somos uma empresa sólida e séria que busca não somente atender a sua necessidade, mas também, fornecer a melhor pinça odontológica. Com a estratégia ideal para fornecer a pinça odontológica, novas parcerias e os próprios processos seletivos se tornam facilitados e otimizados, visto que empresas e talentos buscam pela marca de forma direta, criando um banco de vagas e melhorando a relação dentro do próprio mercado. </p>

<p>A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>

<p>Lembrando que, o respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. Além disso, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas. Está esperando o que para entrar em contato conosco agora mesmo, tirar todas as suas dúvidas e ter a certeza de que fez a escolha certa? Venha fazer um orçamento sem compromisso de nossa pinça odontológica.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>