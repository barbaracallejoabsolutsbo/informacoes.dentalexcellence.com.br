<?php
    $title       = "Kavo dental Excellence";
    $description = "A kavo Dental Excellence é dedicada a fabricação de produtos odontológicos de qualidade, eficiência e modernidade inigualável aos demais. Venha saber mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O kavo Dental Excellence possui o melhor custo benefício do mercado, bem como diversas formas de pagamento para facilitar a aquisição do cliente. Com anos de experiência no ramo, não hesitamos em fazer um bom trabalho e buscamos superar as expectativas de nossos clientes.</p>
<p>Em geral, o kavo Dental Excellence reúne peças de mão essenciais para o dia a dia do profissional de odontologia, além de ser um dos kits acadêmicos mais vendidos no mercado. Mas não se preocupe, pois, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2>Mais sobre kavo Dental Excellence:</h2>
<p>A kavo Dental Excellence é um fabricante de produtos odontológicos com uma gama abrangente de produtos odontológicos que vão desde instrumentos odontológicos habilmente projetados, equipamentos de prática e muito mais.</p>
<p>Em geral, o kavo Dental Excellence é formado pelos principais instrumentos utilizados no dia a dia de um dentista. Nele, podemos encontrar um micromotor, contra ângulo, peça reta, motor de alta rotação ou extra torque e muito mais.</p>
<p>Pois bem, vamos conhecer alguns produtos da kavo Dental Excellence e suas atribuições. Confira:</p>
<ul>
<li>
<p>Expert torque E680L: Possui um sistema antirrefluxo e microfiltro para água do spray, evitando que impurezas cheguem aos canais de água. Além disso, possui cabeça de aço inoxidável, rolamentos de esferas cerâmica e bucha guia em metal duro;</p>
</li>
<li>
<p>Expert torque mini E677L: Possui rotação máxima de 400.000 rpm, irrigação triplo spray e um sistema antirrefluxo e microfiltro para água do spray;</p>
</li>
<li>
<p>Electromatic: A precisão de um preparo com Micromotor Elétrico alemão, agora no seu consultório;</p>
</li>
<li>
<p>Kit solução Multiflex: Instrumento resistente para realização de cirurgias, entre outros.</p>
</li>
</ul>
<p>Lembrando que, os produtos fornecidos pela kavo Dental Excellence vêm em uma embalagem específica, facilitando o transporte e garantindo a longividade e higiene dos aparelhos.</p>
<p>O foco da kavo Dental Excellence são equipamentos e sistemas para laboratórios odontológicos. Com o surgimento do simulador de pacientes, a companhia também ingressa no setor de treinamentos para a área odontológica. </p>
<p>É importante frisar que, de nada adianta você possuir extrema habilidade odontológica se a sua aparelhagem não corresponder. Portanto, não pense em comprar o kit mais barato para economizar, conte com kavo Dental Excellence. </p>
<p>O ideal é garantir o seu kit acadêmico kavo Dental Excellence novo e particular. Nada de ficar dividindo com amigos ou alguém da família, pois cada um tem seu próprio jeito de empunhar.</p>
<p>Para a limpeza adequada do kit kavo Dental Excellence algumas dicas de técnicas básicas e gerais de higienização devem ser realizadas frequentemente em todos os aparelhos. </p>
<p>Pensando em você, confira abaixo uma lista de como realizar a limpeza adequada do kit kavo Dental Excellence. Pois bem:</p>
<ul>
<li>
<p>Faça a limpeza e a lubrificação antes mesmo de colocar a peça de mão na autoclave;</p>
</li>
<li>
<p>Limpe a peça de mão com algodão embebido em álcool, ou em soluções próprias que não danifiquem o material;</p>
</li>
<li>
<p>Embrulhe as peças de mão em papel cirúrgico ou na embalagem própria, protegendo de impurezas e possíveis danos;</p>
</li>
<li>
<p>Introduza as peças para esterilização em autoclave por 20 minutos a 135ºC. Porém, este item pode variar de acordo com o instrumento, vale seguir, também, a orientação do fabricante;</p>
</li>
<li>
<p>Nunca se esqueça de lubrificar as peças de mão antes de colocá-las em operação, pois, ele possibilitará um melhor manuseio e funcionamento na hora do tratamento;</p>
</li>
<li>
<p>Nunca mergulhe ou embeba as peças de mão em soluções desinfetantes, pois pode danificar o material de kavo Dental Excellence.</p>
</li>
</ul>
<p>Ressalta-se que, para o kit kavo Dental Excellence, não é recomendado que seja feita a esterilização em estufas. Não realizar esse procedimento irá evitar assim causar danos nos componentes internos dos materiais.</p>
<h2>A Kavo Dental Excellence:</h2>
<p>A Dental Excellence possui mais de 25 anos de experiência no mercado odontológico, sempre se destacando e oferecendo soluções completas aos clientes. Por isso, desde o primeiro contato com o cliente, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados para a entrega dos produtos de kavo Dental Excellence sejam cumpridos à risca. </p>
<p>Atualmente, somos referência em tratamentos dentários de alta qualidade através da kavo Dental Excellence, com foco na valorização e humanização dos pacientes. No relacionamento com o paciente inovamos sendo uma das únicas clínicas a ter diferenciais próprios, trazendo com isso mais comodidade com o paciente.</p>
<p>É importante destacar que kavo Dental Excellence é fabricada seguindo alguns requisitos, entre eles, podemos citar:</p>
<p>• Valorização do lado humano;<br />• Estabelecimento de metas e objetivos concretos;<br />• Prioridade no bom relacionamento com seu mercado.</p>
<p>Além dos fatores supracitados, ressaltamos que respeitamos e temos responsabilidade social e ambiental, além de uma ética responsável para conduta empresarial, através também, do fornecimento de produtos kavo Dental Excellence.</p>
<p>A escolha dos equipamentos e dos materiais odontológicos está ligada à especialidade da clínica, mas muitos itens fazem parte da rotina de todo consultório. Escolha os produtos de qualidade da kavo Dental Excellence, uma empresa certificada que trabalha com produtos dos melhores fabricantes de materiais odontológicos.</p>
<p>Com foco e determinação, nós conquistamos o nosso espaço no mercado e visamos agregar valores justos a todos os nossos produtos em conjunto com diversas formas de pagamento para facilitar a sua aquisição e parceria. </p>
<p>Vale lembrar que, o respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. E ainda, a nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>Por fim, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Entre em contato conosco agora mesmo, tire todas as suas dúvidas e faça um orçamento sem compromisso. Nós estamos esperando por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>