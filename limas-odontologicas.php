<?php
    $title       = "Limas Odontológicas";
    $description = "A Dental Excellence conta com parceiros e fornecedores de extrema confiança e máxima agilidade para fornecer as limas odontológicas. Venha conferir.
";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Entre o instrumental conhecido como endodôntico, se encontram as limas odontológicas que são introduzidas, em geral, no canal radicular para modificá-lo de alguma forma. Além disso, as limas odontológicas também podem alisar e retificar a curvatura dos canais radiculares.</p>
<p>Pois bem, a Dental Excellence, por sua vez, não hesita em fazer um bom trabalho e sabe da importância das limas odontológicas, por isso, conta com uma equipe unida e organizada que está sempre atenta as atualizações do ramo para fornecer o que há de melhor e mais moderno aos clientes.</p>
<h2>Saiba mais sobre as limas odontológicas:</h2>
<p>Primeiramente, é importante saber quantas vezes se pode utilizar as limas odontológicas para evitar fraturas e acidentes. Os fabricantes recomendam descartar o instrumento depois do primeiro uso, porém é verdade que isto muitas vezes não se realiza na prática, o que pode ocasionar futuros descontentamentos.</p>
<p>No mundo dos instrumentos endodônticos, estão as limas odontológicas usadas para procedimentos de canal radicular. Com o tempo, a tecnologia avança e continua produzindo arquivos melhores, projetados para tratar cada caso clínico com a maior precisão e conforto.</p>
<p>Sem dúvida, a maior dúvida dos dentistas em relação a as limas odontológicas é qual sistema de arquivos seria o mais completo, porém não existe uma resposta perfeita para isso.</p>
<p>Dependendo do caso clínico, um ou outro procedimento precisará ser utilizado, porém, isso também depende muito da metodologia do endodontista, pois alguns são mais conservadores que outros. </p>
<p>Na verdade, muitos endodontistas alternam e combinam procedimentos. No entanto, os procedimentos padrões K permanecem os mais comumente usados ​​e práticos, em oposição aos arquivos mecanizados que são mais caros e frágeis nos segmentos distais.</p>
<p>Vamos ver abaixo as limas odontológicas e as suas características:</p>
<p>Limas odontológicas K:</p>
<p>As mais utilizadas para preparar o canal radicular. Com o tempo têm variado de seção quadrangular em triangular e romboidal, a dar lugar aas limas K-Flex e Flex-R.</p>
<ul>
<li>
<p>De 1.97 a 0.88 estrias cortantes por milímetro.</p>
</li>
<li>
<p>Ângulo helicoidal de 45°.</p>
</li>
<li>
<p>Disponível em longitudes de 21, 25 e 31 mm, entre outros.</p>
</li>
</ul>
<p>Limas odontológicas Flex-R:</p>
<ul>
<li>
<p>Instrumento derivado das limas K acordonadas.</p>
</li>
<li>
<p>Com estrias mais agudas.</p>
</li>
<li>
<p>Ângulo de corte é mais negativo que em uma lima tipo K tradicional e enroscada.</p>
</li>
<li>
<p>Roane eliminou o ângulo de transição da ponta, que faz com que siga mais facilmente o canal sem produzir escalones.</p>
</li>
</ul>
<p>Limas odontológicas K-Colorinox:</p>
<p>São fabricadas em aço de alta qualidade de Suécia muito fino e inoxidável. Fabricados por torsão, os conferem uma alta resistência ante a fratura, a manter suas fibras de metal intactas. Aqui, as limas odontológicas agradam o condutor radicular, seja de maneira abrasiva ou cortante.</p>
<p>Lembrando que, para cada ação existe um modelo específico de limas odontológicas, com designs adequados aos procedimentos.</p>
<p>Atualmente, as limas odontológicas mais utilizadas podem ser limas reciprocantes; rotatórias; manuais; tipo k (kerr ou k-file); flex; hedstroen ou oscilatórias.</p>
<p>De forma técnica, as limas odontológicas, como regra, quando temos uma unidade com a estrutura da coroa íntegra, o ponto escolhido para iniciar o desgaste está localizado na fossa central. De acordo com alguns profissionais podemos também dividir a superfície oclusal em terços nos seguintes sentidos: mésio-distal e vestíbulo-lingual.</p>
<p>Uma das grandes dificuldades da dentística está no domínio das técnicas de limas odontológicas, ao restaurar a aparência natural do elemento dental.</p>
<p>Durante a análise e a escolha para a realização de limas odontológicas em resina composta devemos observar alguns aspectos do elemento a ser restaurado como, por exemplo: a sua matiz ou tonalidade, o seu valor ou luminosidade, a sua opalescência e fluorescência, além de suas características anatômicas.</p>
<p>É relevante lembrar que existem vários tipos de escala de cores e um leque de resinas compostas disponíveis no mercado para limas odontológicas. A dificuldade está em relacionar a escala de cor escolhida com o material restaurador.</p>
<p>Devemos dessa forma solucionar o problema realizando a polimerização de uma pequena porção do material de limas odontológicas</p>
<p> e a posterior comparação com a escala de cor disponível. Teremos uma relação entre a escala e as cores da resina "Y", devemos atentar que ocorre uma discreta variação de escala para escala e de material para material.</p>
<h2>Adquira as limas odontológicas da Dental!</h2>
<p>Há 25 anos atuando no mercado odontológico também de limas odontológicas trabalhamos todos os dias para entregar os melhores produtos com a melhor qualidade. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. </p>
<p>A nossa clínica é moderna e bem equipada com o que há de melhor na área de limas, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>Lembrando que, o respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. Além disso, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes.</p>
<p>A Dental Excellence se destaca por sua capacidade de se reinventar e criar produtos que chegaram ao mercado e expandiram sua presença de maneira expressiva. Devido ao seu compromisso de procurar sempre alcançar o máximo em qualidade com um custo-benefício para o cliente final, a nossa equipe fornece com elevado nível de desenvolvimento tecnológico.</p>
<p>Os nossos serviços possuem outra vantagem bastante valiosa, que é a possibilidade de estender o acesso aos cuidados dentais para os familiares e dependentes dos funcionários, quando contratado um plano odontológico. </p>
<p>Com a estratégia ideal para você, nós contamos com novas parcerias e os próprios processos seletivos se tornam facilitados e otimizados, visto que empresas e talentos buscam pela marca de forma direta, criando um banco de vagas e melhorando a relação dentro do próprio mercado. Ligue agora mesmo e garanta as melhores limas. Não perca mais tempo e deixe os detalhes com a nossa equipe. Esperamos por você.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>