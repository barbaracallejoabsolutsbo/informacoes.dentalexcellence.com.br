<?php
    $title       = "Excelllence Medical";
    $description = "Se você quer saber mais sobre Excellence Medical, entre em contato com a equipe da Dental Excellence a qualquer hora do dia. Esperamos por você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Dental Excellence, também é conhecida como Excellence Medical, ou seja, traz para o mercado o que cada profissional estudou de melhor: a qualidade tanto de conhecimento e técnicas profissionais, quanto de cuidado individualizado e completo das pessoas. Além disso, todos os nossos profissionais oferecem soluções completas aos clientes em conjunto com um atendimento totalmente personalizado. </p>

<p>Ao pé da tradução, a Excellence Medical quer dizer excelência médica, e neste sentido, não há no que se mencionar sobre o profissionalismo da Dental Excellence. Somos uma empresa sólida e séria que possui mais de 25 anos no ramo odontológico, oferecendo produtos modernos e eficientes, de acordo com as constantes evoluções do mercado. </p>

<h2>Excellence Medical:</h2>

<p>Quando falamos sobre Excellence Medical,  independentemente de qual seja, é necessário lembrar que os materiais odontológicos ajudam e até facilitam esse procedimento.</p>

<p>Neste sentido, para uma Excellence Medical é importante saber que a escolha dos materiais é de suma importância antes de sua abertura, para que assim, seja possível atender os pacientes com eficiência. </p>

<p>Em geral, para uma Excellence Medical, os principais materiais utilizados são:</p>

<ul>
<li>
<p>Anestésico com vaso ou anestésico sem vaso;</p>
</li>
<li>
<p>Anestésico tópico;</p>
</li>
<li>
<p>Bicarbonato de sódio;</p>
</li>
<li>
<p>Cariostático;</p>
</li>
<li>
<p>Cimento cirurgico;</p>
</li>
<li>
<p>Cimento endodôntico;</p>
</li>
<li>
<p>Clorexidina 0,12%;</p>
</li>
<li>
<p>Clorexidina 2%;</p>
</li>
<li>
<p>Cones acessórios;</p>
</li>
<li>
<p>Cones guta;</p>
</li>
<li>
<p>Cunhas de madeira;</p>
</li>
<li>
<p>Dessensibilizante;</p>
</li>
<li>
<p>Discos soflex;</p>
</li>
<li>
<p>Dycal, entre muitos outros.</p>
</li>
</ul>

<p>É importante destacar que para uma Excellence Medical impecável, se faz necessário cumprir com a limpeza à risca, tendo em vista que, nem sempre todos os materiais são descartáveis.</p>

<p>Em uma Excellence Medical, após cada atendimento feito, todas as ferramentas que foram usadas devem ser limpas e esterilizadas, feito isto, o material deve ser levado para um local apropriado por meio de bandejas.</p>

<p>Lembrando que, as agulhas devem ser retiradas previamente das seringas e descartadas em um lixo apropriado pós-atendimento e os instrumentos deverão ser embalados em um papel de esterilização apropriado, selados e levados para a autoclave. Esse é o básico para uma Excellence Medical de qualidade.</p>

<p>Lembre-se sempre de utilizar em sua Excellence Medical, luvas de látex para limpeza e luvas de procedimento para embalar os materiais.</p>

<p>O cuidado com a saúde bucal tem impactos em todos os aspectos da nossa vida, desde o bem-estar físico e a disposição emocional, até a qualidade dos relacionamentos sociais. No entanto, esses benefícios odontológicos também são muito importantes no meio profissional de acordo com uma Excellence Medical.</p>

<p>Problemas com a integridade dentária e com a aparência do sorriso causam a falta de concentração, a diminuição no desempenho e ainda faz com que profissionais altamente qualificados não tenham a confiança necessária para comandar uma importante reunião ou concluir uma venda relevante, por exemplo. É aí que entra a Excellence Medical com talentos e profissionais mais capacitados que são naturalmente atraídos por vagas que proporcionam benefícios e vantagens que agregam ainda mais valor ao trabalho e melhoram a sua qualidade de vida.</p>

<h2>Algumas dicas para oferecer uma boa Excellence Medical:</h2>

<ul>
<li>
<p>Valores acessíveis;</p>
</li>
<li>
<p>Aumenta a produtividade;</p>
</li>
<li>
<p>Impactos na reputação da marca;</p>
</li>
<li>
<p>Melhora no ambiente de trabalho, entre outros.</p>
</li>
</ul>

<p>Os serviços de uma Excellence Medical possui outra vantagem bastante valiosa, que é a possibilidade de estender o acesso aos cuidados dentais para os familiares e dependentes dos funcionários, quando contratado um plano odontológico. </p>

<p>Com a estratégia ideal para Excellence Medical, novas parcerias e os próprios processos seletivos se tornam facilitados e otimizados, visto que empresas e talentos buscam pela marca de forma direta, criando um banco de vagas e melhorando a relação dentro do próprio mercado. </p>

<p>Excellence Medical e a Dental Excellence:</p>

<p>Somos uma empresa sólida e séria que busca não somente atender a sua necessidade como uma boa Excellence Medical, mas também, superar todas as suas expectativas. Por isso, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua parceria e contratação.</p>

<p>Percebendo a falta de um ambiente integrado totalmente pensado para o bem-estar do paciente na cidade, resolvemos apostar em oferecer este serviço por conta própria. Desde a recepção do cliente até o final de nossa venda, o acolhimento e atendimento completo é o diferencial que destaca a nossa empresa como Excellence Medical em relação às demais.</p>

<p>Se necessário, oferecemos fácil acesso aos demais profissionais que se fizerem importantes no tratamento. Além disso, oferecemos todos os equipamentos para exames, intervenções, prescrições e orientações de última geração e prática também fazem parte do conjunto de características que os clientes buscam em uma Excellence Medical, se sentindo mais à vontade e voltando sempre que precisar.</p>

<p>Oferecer um atendimento de qualidade, diferenciado e de excelência que atenda às expectativas do cliente é o objetivo principal de uma Excellence Medical. Olhar a necessidade do cliente de forma integral é essencial para um tratamento e cuidado eficaz e de qualidade. </p>

<p>Nós trabalhamos todos os dias para entregar os melhores produtos com a melhor qualidade. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece. O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa.</p>

<p>A Dental Excellence se destaca por sua capacidade de se reinventar e criar produtos que chegaram ao mercado e expandiram sua presença de maneira expressiva. Devido ao seu compromisso de procurar sempre alcançar o máximo em qualidade com um custo-benefício justo para o cliente final, a nossa equipe fornece com elevado nível de desenvolvimento tecnológico.</p>

<p>Por fim, nós salientamos ainda que, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes. No momento em que entrar em contato conosco, você notará que encontrou a empresa ideal. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. Ligue agora mesmo e faça um orçamento.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>