<?php
    $title       = "Excellence Materiais Odontológicos";
    $description = "Se você quer saber mais sobre Excellence Medical, entre em contato com a equipe da Dental Excellence a qualquer hora do dia. Esperamos por você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Dental <strong>Excellence materiais odontológicos </strong>possui mais de 25 anos no ramo odontológico, oferecendo os melhores e mais modernos produtos, tendo em vista que, conta com uma equipe unida e organizada que está sempre atenta as atualizações do mercado para fornecer o que há de melhor e mais moderno aos clientes.</p>
<p>É importante informar que na <strong>Excellence materiais odontológicos</strong>, todos os clientes tem um atendimento personalizado, bem como podem tirar todas as suas dúvidas a qualquer hora do dia, tendo o suporte completo, com a presteza e atenção que você procura e merece.</p>
<h2><strong>Detalhes sobre Excellence materiais odontológicos:</strong></h2>
<p>A <strong>Excellence materiais odontológicos </strong>entende a importância de fornecer produtos de qualidade. Por isso, vamos entender melhor este ramo.</p>
<p>Pois bem, a <strong>Excellence materiais odontológicos </strong>os utiliza em diversos tratamentos, como remoção de placa bacteriana, tratamento de cáries, intervenções estéticas, reconstruções dentárias e implantes.</p>
<p>Na verdade, a <strong>Excellence materiais odontológicos</strong> pode ser feita de diversas fontes, desde ligas metálicas a polímeros com memória de forma. Vale salientar que, a <strong>Excellence materiais odontológicos </strong>possui uma vantagem de usar os polímeros em materiais odontológicos com sua alta biocompatibilidade e durabilidade tanto em implantes como em procedimentos de restauração, entre outros</p>
<p>Novos materiais odontológicos surgem no mercado e muitas vezes o dentista fica confuso sobre a escolha certa. A <strong>Excellence materiais odontológicos, </strong>por sua vez, visa fornecer  nanopartículas, nanoesferas, nanobastões, nanotubos, nanofibras, dendrímeros e copolímeros dendríticos, cada um com propriedades únicas que encontram aplicações em materiais dentários.</p>
<p>Os materiais dentários estão sujeitos a muitos ciclos de tensões repetidas durante a mastigação. Os materiais sujeitos a essa tensão sofrem fadiga do material, o que significa que eles fraturam com uma tensão inferior ao valor necessário para uma única aplicação de carga. É o limite de resistência, ou seja, ciclos de estresse máximo que podem ser mantidos sem falhas. Assim, a resistência da <strong>Excellence materiais odontológicos</strong> à fadiga é definida como a tensão na qual o material combate sob carregamento repetido.</p>
<p>A <strong>Excellence materiais odontológicos</strong> desempenha um papel fundamental na reabilitação das estruturas dentais e na regeneração dos tecidos orais. Nos últimos anos, o desenvolvimento tecnológico desempenhou um papel fundamental na área de materiais odontológicos, introduzindo os materiais de engenharia. Por isso, a <strong>Excellence materiais odontológicos</strong> exibe comportamento inteligente que ajuda os materiais dentários a responder aos estímulos, alterando uma ou mais de suas propriedades sem comprometer a proteção do dente e dos tecidos bucais.</p>
<p>Da mesma forma, a <strong>Excellence materiais odontológicos</strong> destinados às restaurações protéticas foram fornecidos com uma estrutura inovadora.  Alguns argumentam que em materiais odontológicos é a tenacidade que é necessária, não a resistência ou dureza. Nesse aspecto, considerando também os baixos valores de adesivo na cimentação, e apesar do amplo uso clínico, a <strong>Excellence materiais odontológicos</strong> desenvolve e introduz na odontologia produtos inovadores</p>
<p>De forma sucinta, a <strong>Excellence materiais odontológicos</strong> é validada para permitir a previsão de resultados de longo prazo. Os materiais dentários convencionais têm sido aquisitivos por muito tempo, o que significa que foram desenvolvidos em outro lugar para diferentes propósitos, e seguindo os princípios de adotar, adaptar, melhorar às vezes com receitas idiossincráticas e pessoais de médicos individuais da <strong>Excellence materiais odontológicos</strong>, foram introduzidos na prática clínica. </p>
<p>Normalmente, na <strong>Excellence materiais odontológicos</strong> os produtos passam por tratamento térmico para atingir a cristalização final em uma temperatura bem definida por um tempo exato, conforme indicado pelos fabricantes. </p>
<p>Lembrando que, a cristalização é uma etapa importante realizada em laboratório pelo técnico de prótese dentária em estufa. Sobre este aspecto, uma contribuição interessante para a utilização de sistemas inovadores para controlar e melhorar os métodos de calibração do forno é fornecida que tem como foco o emprego da tomografia para avaliar as diferenças estruturais da superfície introduzidas pelas variações de temperatura em cerâmicas cristalizadas ou sinterizadas para uso em laboratório odontológico. Por isso, conte com a <strong>Excellence materiais odontológicos</strong>.</p>
<p>Com a ajuda da tecnologia, diversos materiais da <strong>Excellence materiais odontológicos</strong> surgiram no mercado odontológico com uma abordagem inovadora, aplicada à odontologia, promovendo a interação dos constituintes orgânicos ou inorgânicos a nível molecular, permitindo a incorporação do material no complexo da estrutura dentária viva ou favorecendo a implantação e substituição de biomateriais. </p>
<p>Diante de todos esses fatores, a indicação é procure saber mais sobre a Dental <strong>Excellence materiais odontológicos</strong> e tenha informações completas, desde o atendimento inicial até o serviço completo, bem como conheça toda a estrutura que é fornecida ao cliente.</p>
<h2><strong>Bem vindo a Dental Excellence materiais odontológicos!</strong></h2>
<p>Atualmente, nós somos referência em tratamentos dentários de alta qualidade, com foco na valorização e humanização dos pacientes. Além disso, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos a risca.</p>
<p>Com foco e determinação, nós conquistamos o nosso espaço no mercado e visamos agregar valores justos a todos os nossos produtos em conjunto com diversas formas de pagamento para facilitar a sua aquisição e parceria.</p>
<p>Vale lembrar que, o respeito, a lealdade, qualidade, éticas nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. E ainda, a nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia.</p>
<p>Há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para para entregar os melhores produtos com a melhor qualidade. Vale salientar que, todos os nossos profissionais passam por um treinamento adequado para fornecer o atendimento personalizado que o cliente procura e merece.</p>
<p>A escolha dos equipamentos e dos materiais odontológicos está ligada à especialidade da clínica, mas muitos itens fazem parte da rotina de todo consultório. Escolha os produtos de qualidade da Dental Excellence, uma empresa certificada que trabalha com produtos dos melhores fabricantes de materiais odontológicos. Se interessou e quer saber mais? Entre em contato conosco agora mesmo, tire todas as suas dúvidas e tenha a certeza de que fez a escolha certa. Esperamos por você. Venha conferir.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>