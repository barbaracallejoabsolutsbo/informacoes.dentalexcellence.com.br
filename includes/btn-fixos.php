<div class="btn-fixo">

	<a class="btn-telephone" title="Clique e ligue" href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>">

        <i class="fas fa-phone-alt"></i>

    </a>

</div>

<div class="btn-fixo">

	<a class="btn-whatsapp" title="whatsApp" href="http://api.whatsapp.com/send?phone=55<?php echo $unidades[1]["ddd"].$unidades[1]["whatsapp"]; ?>&text=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informa%C3%A7%C3%B5es.">

       	<i class="fab fa-whatsapp"></i>

    </a>

</div>

<div class="btn-fixo">

	<a class="btn-mail" title="E-mail" href="mailto:<?php echo $emailContato; ?>?CC=<?php echo $emailAbsolut; ?>&Subject=Achei%20seu%20site%20no%20Google%20e%20gostaria%20de%20mais%20informações%20-%20Dental_Excellence">

        <i class="fas fa-envelope-open-text"></i>

   	</a>

</div>