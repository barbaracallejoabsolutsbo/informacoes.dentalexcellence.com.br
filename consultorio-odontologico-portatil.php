<?php
    $title       = "Consultório Odontológico Portátil";
    $description = "Conheça o consultório odontológico portátil da Dental Excellence e defina quais os serviços serão oferecidos para o seu tipo de serviço em específico.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Conheça o consultório odontológico portátil da Dental Excellence. Nós atuamos neste ramo há mais de 25 anos, buscando superar as expectativas dos clientes, apresentando soluções completas e eficientes. Além disso, a qualquer hora dia, nós estamos disponíveis para tirar todas as suas dúvidas e fornecer todo o suporte necessário, com presteza e atenção.</p>

<p>De forma sucinta, um consultório odontológico portátil pode ser fabricado em tipo mala em material leve, resistente, contendo rodízios e alças para facilitar o transporte. O consultório odontológico portátil da Dental Excellence é completo, moderno, certificado, permitindo realizar os seus atendimentos a qualquer hora e em qualquer lugar.</p>

<h2>Benefícios do consultório odontológico portátil:</h2>

<p>Em suma, o consultório odontológico portátil serve para atendimento a domicílio, possuindo ajustes de ar e água para as ponteiras de baixa e alta qualidade.</p>

<p>Com a demanda de atendimentos odontológicos cada vez maior, a necessidade de serviços específicos também aumenta, exigindo de vários profissionais seu deslocamento para a realização de consultas, por isso, o consultório odontológico portátil é a solução ideal.</p>
<p>Em geral, o consultório odontológico portátil é um tipo de equipamento flexível, leve e de fácil instalação, necessitando apenas de um ponto de energia elétrica e água potável, dispensando os requisitos básicos de um consultório odontológico padrão.</p>
<p>O consultório odontológico portátil pode ser transportado em qualquer veículo e não necessita de muito espaço para ser montado. Além disso, o consultório odontológico portátil é um grande aliado no atendimento de clientes com necessidades especiais, uma vez que pessoas acamadas e tetraplégicas necessitam de atendimento home care. </p>
<p>É importante destacar que, nem todos os equipamentos utilizados em consultório, são necessários para um atendimento através do consultório odontológico portátil. Por isso, confira alguns itens que se deve levar junto ao consultório odontológico portátil: </p>
<ul>
<li>
<p>Jogo clínico; </p>
</li>
<li>
<p>Fotóforo; </p>
</li>
<li>
<p>Luva; </p>
</li>
<li>
<p>Gorro; </p>
</li>
<li>
<p>Avental descartável, entre outros.</p>
</li>
</ul>
<p>Lembrando que, o consultório odontológico portátil é amplamente usado pelo exército, por exemplo, em projetos com a intenção de proporcionar serviços de saúde em lugares de difícil acesso. </p>
<p>Neste sentido, é melhor utilizar aparelhos que possuam um compressor de ar embutido, já que durante a avaliação dos pacientes, é necessário que os dentes e estruturas orais próximas estejam secos. O sistema de consultório odontológico portátil, sem dúvidas, é uma ótima opção para profissionais que querem ou necessitam adotar um novo estilo de trabalho satisfatório.</p>
<p>O consultório odontológico portátil nada mais é do que uma estrutura compacta e moderna que visa levar serviços de baixa ou média complexidade até o paciente, sendo usado, inclusive em atendimentos de emergência ou em ambientes pequenos.</p>
<p>Obtenha o consultório odontológico portátil e reúna as funções do consultório odontológico tradicional em uma espécie de maleta leve, mas ao mesmo tempo resistente. Os equipamentos podem ser desmontados ou simplesmente fechados para serem levados de um lugar a outro, com peso e tamanho ideal para o transporte.</p>
<p>A possibilidade de ampliação da atuação é um dos principais benefícios deste consultório, pois, com essa estrutura compacta e ao mesmo tempo completa, o profissional dentista consegue atender vários públicos, nos mais diferentes locais. Sendo eles:</p>
<ul>
<li>
<p>Atendimento home care, para idosos, pacientes com necessidades especiais ou com dificuldades de locomoção, entre outros.</p>
</li>
<li>
<p>Atendimento domiciliar, para pacientes com dificuldades de locomoção, pelos mais diferentes motivos. </p>
</li>
</ul>
<p>Além de ser uma estrutura de fácil locomoção, ela permite que o profissional dentista monte seu consultório em qualquer hora e lugar. Além disso, este consultório pode organizar sua rotina, de forma que as consultas do dia aconteçam em regiões próximas, ou até mesmo aproveite o intervalo para resolver alguma demanda pessoal ou profissional.</p>
<p>Importante destacar que o consultório odontológico portátil foi projetado para ser transportado em qualquer veículo. Alguns fabricantes, inclusive, oferecem a possibilidade de personalização da estrutura.</p>
<h2>Adquira já o melhor consultório odontológico portátil!</h2>
<p>Em primeiro lugar, pensando no bem estar completo do cliente, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição do melhor consultório odontológico portátil. Além disso, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados sejam cumpridos à risca. </p>
<p>Há 25 anos atuando no mercado odontológico, trabalhamos todos os dias para entregar os melhores produtos com os melhores produtos para o consultório odontológico portátil. A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>A nossa clínica é moderna e bem equipada com o que há de melhor na área, com sistema de diagnóstico digital. Contamos também com um corpo clínico de dentistas que além da clínica geral, são especialistas nas mais diversas áreas da odontologia. </p>
<p>Atualmente, nós somos referência para tratamentos dentários de alta qualidade, com foco na valorização e humanização dos pacientes. O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. No momento em que entrar em contato com a nossa equipe, você poderá tirar todas as suas dúvidas e terá a certeza de que encontrou a empresa ideal para se tornar o mais novo parceiro de longa data. Deixe os detalhes conosco e desfrute de um trabalho bem feito. </p>
<p>Por fim, a nossa política de qualidade é buscar sempre a excelência em tudo que fazemos, por isso, para que isso aconteça, estamos sempre capacitando nossos colaboradores e estimulando os cirurgiões dentistas, além de constantes investimentos em materiais e equipamentos sempre focando nos pacientes.</p>
<p>Está esperando o que para ligar agora mesmo, tirar todas as suas dúvidas e realizar um orçamento sem compromisso? A qualquer hora do dia estamos disponíveis para fornecer o suporte completo que o cliente procura e merece, com presteza e atenção. Venha conferir o nosso consultório odontológico portátil e tenha a certeza de que fez a escolha certa. A nossa missão é transformar todos os sorrisos do mundo em um só. Ligue agora mesmo e saiba mais sobre os nossos produtos. Esperamos por você.</p>


                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>