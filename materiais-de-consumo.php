<?php
    $title       = "Materiais de Consumo";
    $description = "Garanta já os melhores e mais modernos materiais de consumo da Dental Excellence por valores acessíveis e justos, bem como diversas formas de pagamento.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    <?php /// include "includes/modal-orcamento-qsmi.php"; ?>

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Há 25 anos atuando no mercado odontológico, nos tornamos referência quando o assunto é materiais de consumo. A Dental Excellence treina os seus profissionais para fornecer um atendimento inigualável aos demais, bem como soluções completas e modernas, acompanhando as constantes evoluções do mercado.</p>

<p>Para proporcionar saúde bucal para seus pacientes, além dos equipamentos e instrumentais odontológicos, o profissional precisa conhecer bem os materiais de consumo que são utilizados em cada especialidade odontológica. Por isso, a qualquer hora do dia, nós estamos disponíveis para tirar todas as suas dúvidas.</p>
<h2>Quais são os materiais de consumo mais utilizados?</h2>
<p>Além dos materiais de consumo, há os materiais descartáveis. No entanto, vamos conhecer melhor as funções dos materiais de consumo. Pois bem:</p>

<ul>
<li>
<p>Ácido fosfórico 37%: utilizado em consistência gel para realizar condicionamento ácido em esmalte e dentina; </p>
</li>
<li>
<p>Adesivo dental: biomaterial utilizado para promover a união de materiais de consumo resinosos à substratos, sendo tecidos dentários ou materiais odontológicos;</p>
</li>
<li>
<p>Cimentos odontológicos: biomateriais que possuem uma grande variedade de aplicações e apresentam diferentes composições, agindo por retenção mecânica ou adesão. Porém, para cada procedimento odontológico é indicado um cimento específico em relação aos materiais de consumo;</p>
</li>
<li>
<p>Material de moldagem: biomaterial responsável pela reprodução de estruturas intra orais para a confecção de modelos de estudo e modelos de trabalho;</p>
</li>
<li>
<p>Pino de fibra de vidro: utilizado para dar suporte à restaurações de dentes endodonticamente tratados, bem como ancoragem de coroas protéticas, entre outros.</p>
</li>
</ul>
<p>Lembrando que, é muito importante o dentista ter uma rotina de organização no consultório para preservar a integridade dos equipamentos, instrumentais e materiais de consumo.</p>
<p>Conforme supracitado, além dos materiais de consumo, existem os descartáveis. Entre eles, podemos citar:</p>
<ul>
<li>
<p>Sugador:</p>
</li>
<li>
<p>Agulha descartável:</p>
</li>
<li>
<p>Gaze:</p>
</li>
<li>
<p>Algodão:</p>
</li>
<li>
<p>Lâmina de bisturi:</p>
</li>
<li>
<p>Fio de sutura, entre outros.</p>
</li>
</ul>
<p>É importante frisar que, uma lista de materiais de consumo para adquirir antes da inauguração do seu consultório é mais do que necessário para garantir que, ao iniciar suas atividades, você esteja preparado para tratar de maneira correta seus pacientes. </p>
<p>Com a lista de materiais de consumo básicos para iniciar suas atividades, os outros itens devem ser escolhidos levando em consideração qual a área de especialidade do dentista, trazendo um atendimento de qualidade e uma maior chance de fidelização do cliente.</p>
<p>Os materiais de consumo são essenciais para o dia a dia de qualquer consultório odontológico, devido ao fato dos riscos a que a profissão está exposta. Para evitar qualquer tipo de risco, o profissional deve cumprir todas as normas de biossegurança e sempre manter um estoque de materiais de consumo adequados no consultório.</p>
<p>Os instrumentos para os materiais de consumo também são ferramentas inteiramente essenciais. Portanto, também devem estar presentes no consultório de todos os dentistas. Lembrando que, existem diversos materiais odontológicos, instrumentos e equipamentos considerados essenciais no consultório de qualquer dentista.</p>
<p>Portanto, independentemente de sua especialidade, os materiais de consumo são essenciais para se realizar um trabalho mais profissional. Assim como, para se obter resultados satisfatórios.</p>
<h2>Materiais de consumo estão na Dental Excellence!</h2>
<p>Pois bem, desde o primeiro contato, nós estabelecemos uma relação de transparência e comprometimento para que todos os prazos estipulados aos materiais de consumo sejam cumpridos à risca. Além disso, a nossa equipe trabalha de forma unida e organizada acompanhando as constantes evoluções do mercado para fornecer o que há de melhor e mais moderno.</p>
<p>As nossas dicas em relação aos materiais de consumo são:</p>
<ul>
<li>
<p>Verifique nas embalagens dos materiais de consumo as condições de temperatura e armazenamento indicadas por cada fabricante; </p>
</li>
<li>
<p>Realize a desinfecção dos equipamentos conforme as instruções do manual do fabricante; </p>
</li>
<li>
<p>Os instrumentais que apresentam cabos podem ser identificados com elásticos autoclaváveis; </p>
</li>
<li>
<p>Após a desinfecção e secagem, os instrumentos deverão ser embalados em um papel de esterilização apropriado, selados e levados para a autoclave; </p>
</li>
<li>
<p>Esterilizar os instrumentais por kits de especialidade ou kits de procedimento.</p>
</li>
</ul>
<p>Vale salientar que, pensando em seu bem estar completo, nós visamos agregar valores acessíveis e justos em conjunto com diversas formas de pagamento para facilitar a sua aquisição dos melhores materiais de consumo. </p>
<p>A nossa missão é comercializar produtos que proporcionem mais qualidade de vida ao ser humano. Oferecendo aos clientes uma linha de produtos reconhecida pela sua qualidade e com excelente custo benefício. Além de garantir a remuneração adequada daqueles que investem e trabalham pela empresa.</p>
<p>O respeito, a lealdade, qualidade, ética nas relações, responsabilidade nas ações e compromisso com prazos são fatores indispensáveis a nossa filosofia de empresa. Trabalhamos todos os dias para entregar os melhores produtos com os melhores preços e com a qualidade de atendimento que o cliente merece, contando com uma equipe treinada e especializada na área para que possam tirar as dúvidas sobre o nosso catálogo de produtos.</p>
<p>A Dental Excellence se destaca por sua capacidade de se reinventar e criar produtos que chegaram ao mercado e expandiram sua presença de maneira expressiva. Devido ao seu compromisso de procurar sempre alcançar o máximo em qualidade com um custo benefício justo para o cliente final, a nossa equipe fornece com elevado nível de desenvolvimento tecnológico.</p>
<p>Com a estratégia ideal para você, nós contamos com novas parcerias e os próprios processos seletivos se tornam facilitados e otimizados, visto que empresas e talentos buscam pela marca de forma direta, criando um banco de vagas e melhorando a relação dentro do próprio mercado. Deixe os detalhes conosco e desfrute de um trabalho bem feito.</p>
<p>Além dos fatores supracitados, ressaltamos que respeitamos e temos responsabilidade social e ambiental, além de uma ética responsável para conduta empresarial. E ainda, nós oferecemos aos clientes uma linha de produtos reconhecida pela sua qualidade e com excelente custo benefício. Além de garantir a remuneração adequada daqueles que investem e trabalham pela empresa. Não perca mais tempo e nem a oportunidade de se tornar parceiro de uma empresa que prioriza e respeita você. No momento em que entrar em contato com a nossa equipe, você notará que fez a escolha certa. Entre em contato agora mesmo e faça um orçamento sem compromisso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>